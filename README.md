<!-- PROJECT LOGO -->
<br />

<p align="center">
  <a href="https://gitlab.com/TS-open-source/petzz-backend/">
    <img src="https://i.imgur.com/elA2NPl.png" alt="Logo">
  </a>

  <h1 align="center">Petzz Api</h1>

<!-- ABOUT THE PROJECT -->
## About The Project

Petzz is a platform where you can register a pet for adoption and list them based on geolocation, clearly describing to users the characteristics of your pet helping others find a new friend.

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/TS-open-source/petzz-backend.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```



<!-- USAGE EXAMPLES -->
## Usage
<!--

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

-->
_For more examples, please refer to the [Documentation](https://example.com)_
> Comming Soon



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/TS-open-source/petzz-backend/issues) for a list of proposed features (and known issues).

